package ru.tsc.apozdnov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.ProjectDtoModel;

@NoArgsConstructor
public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final ProjectDtoModel project) {
        super(project);
    }

}