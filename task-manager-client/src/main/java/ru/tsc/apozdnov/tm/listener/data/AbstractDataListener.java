package ru.tsc.apozdnov.tm.listener.data;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.apozdnov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.apozdnov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractDataListener extends AbstractListener {

    @Autowired
    public IDomainEndpoint domainEndpoint;

}