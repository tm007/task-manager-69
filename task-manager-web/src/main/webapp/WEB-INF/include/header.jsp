<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<h1><a href="/">Менеджер задач</a></h1>
<head>
    <title>${viewName}</title>
    <style>
        h1 {
            font-family: "Times New Roman";
        }

        td {
            padding: 5px;
            border: navy dashed 1px;
        }

        th {
            font-weight: 700;
            text-align: left;
            background: darkgoldenrod;
            border: black solid 1px;
        }

        ul.hr {
            margin: 0;
            padding: 4px;
        }

        ul {
            display: flex;
            /*justify-content: space-between;*/
            padding: 0;
            margin: 0;
        }

        li {
            width: 4%;
            margin: 0;
            display: inline-block;
        }

    </style>
</head>
<body>
<ul class="hr">
    <li>
        <form action="/tasks">
            <input type="submit" value="Задачи"></form>
    </li>
    <li>
        <form action="/projects">
            <input type="submit" value="Проекты"></form>
        </form>
    </li>
    <li>
        <sec:authorize access="!isAuthenticated()">
            <form action="/login">
                <input type="submit" value="Залогинься...">
            </form>
        </sec:authorize>
    </li>
    <li>
        <sec:authorize access="isAuthenticated()">
            <form action="/logout">
                <input type="submit" value="Выходи...">
            </form>
        </sec:authorize>
    </li>

</ul>
<hr>