package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class CheckAuthResult {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public CheckAuthResult(@NotNull final Boolean success) {
        this.success = success;
    }

    public CheckAuthResult(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }

}