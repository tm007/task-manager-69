package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.exception.field.EmptyLoginException;
import ru.tsc.apozdnov.tm.exception.field.EmptyPasswordException;
import ru.tsc.apozdnov.tm.model.Role;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.repository.UserRepository;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;


    @Modifying
    @Transactional
    public User createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        @NotNull final Role role = roleType == null ? new Role(user, RoleType.USUAL) : new Role(user, roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
        return user;
    }

    @NotNull
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findFirstByLogin(login).orElse(createUser("admin", "admin", RoleType.ADMIN));
    }

}
