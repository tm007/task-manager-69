package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receiveLog(@NotNull MessageListener listener);

}