package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.apozdnov.tm.api.service.dto.IUserDtoService;
import ru.tsc.apozdnov.tm.config.ContextConfig;
import ru.tsc.apozdnov.tm.dto.model.UserDtoModel;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.exception.entity.UserNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.*;

public class UserServiceTest {

    @NotNull
    private IUserDtoService userService;

    private long INITIAL_SIZE;

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ContextConfig.class);
        userService = context.getBean(IUserDtoService.class);
        userService.create("user", "user", "user@user.ru");
        INITIAL_SIZE = userService.getCount();
    }

    @After
    public void end() {
        userService.clear();
    }

    @Test
    public void create() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.create("", "12345"));
        Assert.assertThrows(ExistsLoginException.class, () -> userService.create("user", "12345"));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.create("test", ""));
        @NotNull final UserDtoModel user = (userService.create("test", "12345"));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getCount());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void createWithEmail() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.create("", "12345", "test@test"));
        Assert.assertThrows(ExistsLoginException.class,
                () -> userService.create("user", "12345", "test@test"));
        Assert.assertThrows(EmptyPasswordException.class,
                () -> userService.create("test", "", "test@test"));
        Assert.assertThrows(ExistsEmailException.class,
                () -> userService.create("test", "12345", "user@user.ru"));
        @NotNull final UserDtoModel user = (userService.create("test", "12345", "test@test"));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getCount());
        Assert.assertEquals(Role.USUAL, user.getRole());
        Assert.assertNotNull(user.getEmail());
    }

    @Test
    public void createWithRole() {
        Assert.assertThrows(EmptyLoginException.class,
                () -> userService.create("", "12345", Role.ADMIN));
        Assert.assertThrows(ExistsLoginException.class,
                () -> userService.create("user", "12345", Role.ADMIN));
        Assert.assertThrows(EmptyPasswordException.class,
                () -> userService.create("test", "", Role.ADMIN));
        @NotNull final UserDtoModel user = (userService.create("test", "12345", Role.ADMIN));
        Assert.assertEquals(INITIAL_SIZE + 1, userService.getCount());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.findByLogin(""));
        Assert.assertNotNull(userService.findByLogin("user"));
        Assert.assertNull(userService.findByLogin("not_user"));
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmptyEmailException.class, () -> userService.findByEmail(""));
        Assert.assertNotNull(userService.findByEmail("user@user.ru"));
        Assert.assertNull(userService.findByEmail("not_user@user.ru"));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist("user"));
        Assert.assertFalse(userService.isLoginExist("not_user"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist("user@user.ru"));
        Assert.assertFalse(userService.isEmailExist("not_user@user.ru"));
        Assert.assertFalse(userService.isEmailExist(""));
        Assert.assertFalse(userService.isEmailExist(null));
    }

    @Test
    @Ignore
    public void remove() {
        @NotNull final UserDtoModel user = userService.findByLogin("user");
        userService.remove(user);
        Assert.assertEquals(INITIAL_SIZE - 1, userService.getCount());
        Assert.assertNull(userService.findByLogin(user.getLogin()));
    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        userService.removeByLogin("user");
        Assert.assertEquals(INITIAL_SIZE - 1, userService.getCount());
        Assert.assertNull(userService.findByLogin("user"));
    }

    @Test
    public void setPassword() {
        @NotNull final UserDtoModel user = userService.create("test", "test");
        @NotNull final String oldPasswordHash = user.getPasswordHash();
        Assert.assertThrows(EmptyIdException.class, () -> userService.setPassword("", "123"));
        Assert.assertThrows(EmptyPasswordException.class, () -> userService.setPassword(user.getId(), ""));
        userService.setPassword(user.getId(), "12345");
        @NotNull final UserDtoModel userAfterUpdate = userService.findByLogin("test");
        Assert.assertNotEquals(oldPasswordHash, userAfterUpdate.getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull final UserDtoModel user = userService.create("test", "test");
        @NotNull final String userId = user.getId();
        Assert.assertThrows(EmptyIdException.class,
                () -> userService.updateUser("", "", "", ""));
        @NotNull final String firstName = "first";
        @NotNull final String lastName = "last";
        @NotNull final String middleName = "middle";
        userService.updateUser(userId, firstName, lastName, middleName);
        @NotNull final UserDtoModel userAfterUpdate = userService.findByLogin("test");
        Assert.assertNotNull(userAfterUpdate.getFirstName());
        Assert.assertNotNull(userAfterUpdate.getLastName());
        Assert.assertNotNull(userAfterUpdate.getMiddleName());
        Assert.assertEquals(firstName, userAfterUpdate.getFirstName());
        Assert.assertEquals(lastName, userAfterUpdate.getLastName());
        Assert.assertEquals(middleName, userAfterUpdate.getMiddleName());
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        @NotNull final UserDtoModel user = userService.create("test", "test");
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(user.getLogin());
        @NotNull final UserDtoModel userAfterUpdate = userService.findByLogin("test");
        Assert.assertTrue(userAfterUpdate.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(EmptyLoginException.class, () -> userService.removeByLogin(""));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin("not_user"));
        @NotNull final UserDtoModel user = userService.create("test", "test");
        userService.lockUserByLogin(user.getLogin());
        userService.unlockUserByLogin(user.getLogin());
        @NotNull final UserDtoModel userAfterUpdate = userService.findByLogin("test");
        Assert.assertFalse(userAfterUpdate.getLocked());
    }

}