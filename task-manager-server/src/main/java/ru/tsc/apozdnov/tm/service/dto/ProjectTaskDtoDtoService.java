package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.apozdnov.tm.api.service.dto.ITaskDtoService;
import ru.tsc.apozdnov.tm.dto.model.TaskDtoModel;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;

import java.util.List;

@Service
public class ProjectTaskDtoDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @Override
    public void bindTaskToProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDtoModel task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskService.update(task);
    }

    @Override
    public void unbindTaskFromProject(@NotNull String userId, @NotNull String projectId, @NotNull String taskId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId.isEmpty()) throw new EmptyIdException();
        if (taskId.isEmpty()) throw new EmptyIdException();
        @Nullable final TaskDtoModel task = taskService.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskService.update(task);
    }

    @Override
    public void removeProjectById(@NotNull String userId, @NotNull String projectId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final List<TaskDtoModel> taskList = taskService.findAllByProjectId(userId, projectId);
        taskList.forEach(taskService::remove);
    }

}