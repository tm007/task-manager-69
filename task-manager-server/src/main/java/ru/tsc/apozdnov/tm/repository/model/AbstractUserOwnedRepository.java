package ru.tsc.apozdnov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

@Repository
public interface AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> {


}