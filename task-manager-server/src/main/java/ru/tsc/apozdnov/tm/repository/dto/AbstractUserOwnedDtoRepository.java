package ru.tsc.apozdnov.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.dto.model.AbstractUserOwnedDtoModel;

@Repository
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedDtoModel> extends AbstractDtoRepository<M> {

}